
import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.scss";
import Hero from "@/components/Hero";
import NodeOverlay from "@/components/NodeOverlay";
import UseCase from "@/components/UseCase";
import AppLinks from "@/components/AppLinks";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Head>
        <title>Piam_Piam</title>
        <meta name="description" content="Piam_Piam by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={`${styles.main} `}>
        <NodeOverlay />

        <Hero
          title="Transformez votre voyage en opportunité!"
          subtitle="Disposez-vous d'espace libre dans vos bagages? Mettez-le à profit! Besoin d'envoyer quelque chose? Trouvez un voyageur!"
        />

        {/*  */}
        < AppLinks />
        {/*  */}

        <div id={styles.section_features}>
          <h2 className="h2-lg my-lg text-center">Comment PIAM-PAIM Est Unique</h2>

          <div className="container">
            <div className="row justify-content-md-center">
              <div className={`${styles.backsplash_container} col-md-4 text-center`} >
                <div className={`${styles.backsplash} mx-auto`}></div>
                <Image
                  src="/images/phones/16.png"
                  alt="Screenshot of Roads app showing the main player with several comments."
                  height={400}
                  width={200}
                  className={`${styles.backsplash_image} phone_shadow`}
                />
              </div>

              <div className='col-md-4 offset-md-1 my-auto'>
                <h3>Annoncez Votre Voyageo</h3>
                <p>Précisez votre date de départ, destination et l'espace disponible dans vos bagages.</p>
              </div>
            </div>

            <div className="row my-5 justify-content-md-center">
              <div className={`${styles.backsplash_container} col-md-4 offset-md-1 text-center`} >
                <div className={`${styles.backsplash} mx-auto`}></div>
                <Image
                  src="/images/phones/24.png"
                  alt="Screenshot of Roads app showing the home page where new comments left by other users can be listened to."
                  height={400}
                  width={200}
                  className={`${styles.backsplash_image} phone_shadow`}
                />
              </div>
              <div className='col-md-4 my-auto order-md-first'>
                <h3>Réservez De L'espace</h3>
                <p>Trouvez un voyageur se rendant à votre destination souhaitée et réservez l'espace nécessaire.</p>
              </div>
            </div>

            <div className="row justify-content-md-center">
              <div className={`${styles.backsplash_container} col-md-4 text-center`} >
                <div className={`${styles.backsplash} mx-auto`}></div>
                <Image
                  src="/images/phones/15.png"
                  alt="Screenshot of Roads app showing how to select friends to add to a channel."
                  height={400}
                  width={200}
                  className={`${styles.backsplash_image} phone_shadow`}
                />
              </div>

              <div className='col-md-4 offset-md-1 my-auto'>
                <h3>Livrez et Recevez</h3>
                <p>Rencontrez le voyageur, remettez-lui votre colis et il le livrera à votre proche à destination.</p>
              </div>
            </div>

          </div>
        </div>

        <div className='text-center'>
          <h2 className="h2-lg my-lg my-lg">Regardez La Démo</h2>
          
          <iframe 
            className="youtube-video"
            src="https://www.youtube.com/embed/9lzJthdMQLc" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            loading='eager'
            >
          </iframe>
        </div>

        {/*  */}
        <div id={styles.section_use_cases}>
          <h2 className="h2-lg my-lg text-center">Pourquoi utiliser PIAM-PIAM?</h2>
          <div className="container">
            <div className="row">
              <UseCase
                imagePath="/images/icons/note.svg"
                title="Postez votre annonce ou réservez de l'espace en quelques clics"
              />
              <UseCase
                imagePath="/images/icons/chat.svg"
                title="Vos colis livrés rapidement par des voyageurs de confiance"
              />
              <UseCase
                imagePath="/images/icons/book.svg"
                title="Économisez sur les frais de livraison traditionnels"
              />
              <UseCase
                imagePath="/images/icons/lecture.svg"
                title="Restez informé à chaque étape, de la réservation à la livraison, grâce à nos notifications instantanées."
              />
              <UseCase
                imagePath="/images/icons/team.svg"
                title="Toutes les transactions sont traitées de manière transparente et sécurisée sur notre plateforme"
              />
              <UseCase
                imagePath="/images/icons/podcast.svg"
                title="Rejoignez une communauté de confiance"
              />
            </div>
          </div>
        </div>
        {/*  */}
        <div id={styles.section_wait_list}>
          <h2 className='h2-lg my-lg text-center'>Télécharger sur</h2>
          < AppLinks />
        </div>

      </main>
    </>
  );
}
