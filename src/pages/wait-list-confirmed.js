import Hero from '@/components/Hero'
import { Head } from 'next/document'
import React from 'react'

export default function WaitListConfirmed() {
    return (
        <>
            <div>
                <Head>PIAM-PIAM Wait List Confirmation</Head>

                <Hero
                title= "You're on the list"
                subtitle= "As soom as Roads is launched you' ll be the first to know!"
                />
            </div>

            <div className= "my-5" ></div>
        </>

    )
}


